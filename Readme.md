# Kudi Message Queue Client JS

This library is a JS Message Queue Library by Kudi. 
It is designed to make it easy to be used for many of the different workers that we have
by providing a consistent interface across all our Queue Providers (Azure and AWS for now :smirk:).

## Getting Started

Let's just jump to a sample code jare. To better understand the interface exposed by the queue, take a look at
the Adapter in `src/adapters/Adapter.js`. Now here is a sample usage code

```javascript

const QueueClient = require('index.js') // or require('message_queue_client_js') if you are loading npm

const adapter = 'azure' // can be aws too
const connectionString = 'AZURE_CONNECTION_STRING_OR_AWS_QUEUE_URL'

const queue = QueueClient({ connectionString: '' }, 'azure')

// use the various interface exposed by Adapter. eg
queue.receiveMessage(...)

```

## AWS Usage
If you are planning to use AWS as the adapter, please refer to this guide [here](https://docs.aws.amazon.com/sdk-for-javascript/v2/developer-guide/loading-node-credentials-environment.html) 
on how to setup credentials for authentication.