/**
 *
 * Take a look at Adapter to see more details about the methods exported.
 *
 * @author Perfect Makanju <perfect@kudi.ai>
 */

const AWS = require('aws-sdk');

/**
 *
 *
 * @param {string} connectionString Queue Url
 * @param {string} region Aws Region Queue is located
 * @param {AWS} client
 * @constructor
 */
function AWSAdapter({ connectionString, region }, client = AWS) {

    client.config.update({ region });

    const sqs = new client.SQS({apiVersion: '2012-11-05'});

    /**
     * Registers a callback to accept the next message on the queue.
     * Note that releaseMessage or deleteMessage should be called as appropriate
     * after callback is done.
     *
     * @param queueName
     * @param callback
     * @param options
     */
    function peekQueueMessage(queueName, callback, options = {}) {
        const params = Object.assign({
            AttributeNames: [
                "SentTimestamp"
            ],
            MaxNumberOfMessages: 1,
            MessageAttributeNames: [
                "All"
            ],
            QueueUrl: connectionString
        }, options);

        sqs.receiveMessage(params, function(err, data) {
            if (err) {
                callback(err)
            } else if (data.Messages) {
                const message = data.Messages[0]
                message.body = message.Body // for standard accessor across all adapters
                callback(null, message)
            }
        });
    }

    /**
     * Push a message to the queue
     *
     * @param {string} queue
     * @param {string} message
     * @param {function} callback
     * @param {object} options
     */
    function sendQueueMessage(queue, message, callback, options = {}) {
        const params = Object.assign({
            DelaySeconds: 10,
            MessageBody: message,
            QueueUrl: connectionString,
        }, options);

        console.log('Sending message to queue:', params.QueueUrl);
        console.log('Message Body:', params.MessageBody);
        sqs.sendMessage(params, function(err, data) {
            // data's structure: { messageId }
            callback(err, data)
        });
    }

    /**
     * Release a message back to the queue
     *
     */
    function releaseMessage(message, callback) {
        // doesn't do anything because aws will automatically make the
        // visible after default visibility timeout expires.
        callback()
    }

    /**
     * Delete a message that has been peeked at.
     *
     *
     * @param {*} message
     * @param {*} callback
     */
    function deleteMessage(message, callback) {
        const deleteParams = {
            QueueUrl: connectionString,
            ReceiptHandle: message.ReceiptHandle
        };
        sqs.deleteMessage(deleteParams, function(err, data) {
            callback(err, data)
        });
    }

    return {
        peekQueueMessage,
        sendQueueMessage,
        releaseMessage,
        deleteMessage,
    }
}

module.exports = AWSAdapter