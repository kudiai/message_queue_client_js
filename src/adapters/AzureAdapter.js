/**
 *
 * Take a look at Adapter to see more details about the methods exported.
 *
 * @author Perfect Makanju <perfect@kudi.ai>
 */

const azure = require('azure');

/**
 *
 *
 * @param {string} connectionString
 * @param {azure} client
 * @constructor
 */
function AzureAdapter({ connectionString }, client = azure) {
    const serviceBusService = client.createServiceBusService(connectionString)

    /**
     * Registers a callback for items in the queue.
     * onQueueMessage is meant to automatically release message back to queue if an error or timeout occurs
     * It should also call delete message if callback process with error.
     *
     * Use peekQueueMessage if you want greater control
     *
     * @param {string} queueName
     * @param {string} callback
     * @param {object} options
     */
    function onQueueMessage(queueName, callback, options = {}) {
        serviceBusService.receiveQueueMessage(queueName, options, callback)
    }

    function peekQueueMessage(queueName, callback, options = {}) {
        serviceBusService.receiveQueueMessage(queueName, Object.assign(options, { isPeekLock: true }), callback)
    }

    /**
     * Push a message to the queue
     *
     * @param {string} queue
     * @param {string} message
     * @param {function} callback
     * @param {function} options
     */
    function sendQueueMessage(queue, message, callback, options) {
        serviceBusService.sendQueueMessage(queue, message, callback)
    }

    /**
     * Subscribe to topic publishes
     *
     * @param {string} topic
     * @param {string} subscriptionTag
     * @param {function} callback
     */
    function onTopicMessage(topic, subscriptionTag, callback) {
        serviceBusService.receiveSubscriptionMessage(topic, subscriptionTag, callback)
    }

    /**
     * publish a message to a topic
     *
     * @param {string} topic
     * @param {string} message
     * @param {function} callback
     */
    function sendTopicMessage(topic, message, callback) {
        serviceBusService.sendTopicMessage(topic, message, callback)
    }

    /**
     * Release a message back to the queue
     *
     */
    function releaseMessage(message, callback) {
        serviceBusService.unlockMessage(message, callback)
    }

    /**
     * Delete a message that has been peeked at.
     *
     *
     * @param {*} message
     * @param {*} callback
     */
    function deleteMessage(message, callback) {
        serviceBusService.deleteMessage(message, callback)
    }

    return {
        onQueueMessage,
        peekQueueMessage,
        sendQueueMessage,
        onTopicMessage,
        sendTopicMessage,
        releaseMessage,
        deleteMessage,
    }
}

module.exports = AzureAdapter