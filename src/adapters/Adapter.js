/**
 * Interface for Queue Adapter.
 *
 * Any implementation of an adapter should implement only the functions
 * and register its implementation with AdapterFactory.
 *
 * @author Perfect Makanju <perfect@kudi.ai>
 */

/**
 * Helper function for no-op :)
 *
 */
function noop() {
    throw new Error('No operation')
}

/**
 * Registers a callback for items in the queue.
 * onQueueMessage is meant to automatically release message back to queue if an error or timeout occurs
 * It should also call delete message if callback process with error.
 *
 * Use peekQueueMessage if you want greater control
 *
 * @param {string} queueName
 * @param {string} callback
 * @param {object} options
 */
function onQueueMessage(queueName, callback, options = {}) {
    noop()
}

/**
 * Registers a callback to accept the next message on the queue.
 * Note that releaseMessage or deleteMessage should be called as appropriate
 * after callback is done.
 *
 * @param queueName
 * @param callback
 * @param options
 */
function peekQueueMessage(queueName, callback, options = {}) {
    noop()
}

/**
 * Push a message to the queue
 *
 * @param {string} queue
 * @param {string} message
 * @param {function} callback
 * @param {object} options
 */
function sendQueueMessage(queue, message, callback, options = {}) {
    noop()
}

/**
 * Subscribe to topic publishes
 *
 * @param {string} topic
 * @param {string} subscriptionTag
 * @param {function} callback
 */
function onTopicMessage(topic, subscriptionTag, callback) {
    noop()
}

/**
 * publish a message to a topic
 *
 * @param {string} topic
 * @param {string} message
 * @param {function} callback
 */
function sendTopicMessage(topic, message, callback) {
    noop()
}

/**
 * Release a message back to the queue
 *
 * @param {string} message
 * @param {function} callback
 */
function releaseMessage(message, callback) {
    noop()
}

/**
 * Delete a message that has been peeked at.
 *
 *
 * @param {string} message
 * @param {function} callback
 */
function deleteMessage(message, callback) {
    noop()
}

module.exports = {
    onQueueMessage,
    peekQueueMessage,
    sendQueueMessage,
    onTopicMessage,
    sendTopicMessage,
    releaseMessage,
    deleteMessage,
}