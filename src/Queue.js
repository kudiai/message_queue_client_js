
const AdapterFactory = require('./AdapterFactory');


/**
 * `config` is specific to adapter. but must contain connection string for all instances
 *
 * The default adapter is set to azure for backward compatibility.
 *
 * @param {object} config
 * @param {string} adapter
 */
module.exports = function createQueue(config, adapter = 'azure') {
    if (!config.connectionString) {
        throw new Error('No connection string specified for Queue')
    }

    return AdapterFactory(adapter, config)
}
