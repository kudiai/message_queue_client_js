/**
 *
 * @auther Perfect Makanju <perfect@kudi.ai>
 */
const AdapterInterface = require('./adapters/Adapter')
const AzureAdapter = require('./adapters/AzureAdapter')
const AWSAdapter = require('./adapters/AWSAdapter')

const Factory = {
    azure(config) {
        return AzureAdapter(config)
    },

    aws(config) {
        return AWSAdapter(config)
    }
}

function AdapterFactory(adapter, config) {
    if (!Factory.hasOwnProperty(adapter)) {
        throw new Error(`Unknown Adapter '${adapter}' passed to factory`)
    }

    const factory = Factory[adapter]
    return Object.assign(Object.create(AdapterInterface), factory(config))
}

module.exports = AdapterFactory
